@extends('frontend.layouts.app')
@section('title', 'index')
@section('content')
    <div class="product-model">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{{route('frontend.index')}}">Trang chủ</a></li>
                <li class="active">Sản phẩm</li>
            </ol>
            <h2>Tất cả sản phẩm</h2>
            <div class="col-md-9 product-model-sec">
                @forelse($products as $product)
                    <a href="{{route('frontend.product.detail', $product->id)}}">
                        <div class="product-grid love-grid">
                            <div class="more-product"><span> </span></div>
                            <div class="product-img b-link-stripe b-animate-go  thickbox">
                                <img src="{{asset('storage/product/'.$product->image)}}" class="img-responsive" alt="{{$product->name}}"/>
                                <div class="b-wrapper">
                                    <h4 class="b-animate b-from-left  b-delay03">
                                        <button class="btns"><span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>Chi tiết</button>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="product-info simpleCart_shelfItem">
                        <div class="product-info-cust prt_name">
                            <h4>{{$product->name}}</h4>
                            <p>ID: {{$product->id}}</p>
                            <span class="item_price">{{number_format($product->price,0,",",".")}} vnđ</span>
                            <div style="margin-bottom: 20px;"></div>
                            <a href="{{route('cart.product', $product->id)}}" class="item_add items">Thêm giỏ hàng</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                @empty
                    <div><h2 class="text-center">Không có sản phẩm nào</h2></div>
                @endforelse
            </div>
            <div class="rsidebar span_1_of_left">
                <section class="sky-form">
                    <div class="product_right">
                        <h4 class="m_2"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Danh mục</h4>
                        <div class="tab2">
                            <ul class="place">
                                <li class="sort">Tất cả danh mục</li>
                                <div class="clearfix"></div>
                            </ul>
                            <div class="single-bottom">
                                @forelse($categories as $category)
                                    <a href="{{route('frontend.product', $category->id)}}"><p>{{$category->name}}</p></a>
                                @empty
                                    <a><p>Không có danh mục nào</p></a>
                                @endforelse
                            </div>
                        </div>
                    </div>
                        <!--script-->
                        <script>
                            $(document).ready(function () {
                                $(".tab2 .single-bottom").hide();

                                $(".tab1 ul").click(function () {
                                    $(".tab1 .single-bottom").slideToggle(300);
                                    $(".tab2 .single-bottom").hide();
                                    $(".tab3 .single-bottom").hide();
                                    $(".tab4 .single-bottom").hide();
                                    $(".tab5 .single-bottom").hide();
                                })
                                $(".tab2 ul").click(function () {
                                    $(".tab2 .single-bottom").slideToggle(300);
                                    $(".tab1 .single-bottom").hide();
                                    $(".tab3 .single-bottom").hide();
                                    $(".tab4 .single-bottom").hide();
                                    $(".tab5 .single-bottom").hide();
                                })
                            });
                        </script>
                        <!-- script -->
                </section>
                <!---->
                <script type="text/javascript" src="{{asset('web/js/jquery-ui.min.js')}}"></script>
                <link rel="stylesheet" type="text/css" href="{{asset('web/css/jquery-ui.css')}}">
                <script type='text/javascript'>//<![CDATA[
                    $(window).load(function () {
                        $("#slider-range").slider({
                            range: true,
                            min: 0,
                            max: 400000,
                            values: [8500, 350000],
                            slide: function (event, ui) {
                                $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                            }
                        });
                        $("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

                    });//]]>
                </script>
                <!---->
            </div>
        </div>
    </div>
    </div>
@endsection